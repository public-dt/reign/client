import React, { useState, useEffect } from "react";
import axios from "axios";
import moment from "moment";
import { List, ListItem, ListItemText } from "@material-ui/core";
import { Delete } from "@material-ui/icons";

const Posts = () => {
  const [posts, setPosts] = useState([]);

  const fetchPosts = async () => {
    const res = await axios("http://localhost:3000/post");

    const data = res.data;

    setPosts(data);
  };

  const deletePost = async id => {
    await axios.put(`http://localhost:3000/post/${id}`, {
      isPublished: false
    });

    fetchPosts();
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  const ListItemLink = props => {
    return <ListItem button {...props} />;
  };

  return (
    <List>
      {posts
        .filter(post => post.story_title || post.title)
        .map(post => {
          return (
            <ListItemLink key={post.objectID}>
              <ListItemText
                primary={post.story_title ? post.story_title : post.title}
                secondary={`- ${post.author} -`}
              />
              <ListItemText
                primary={moment(post.created_at).calendar(null, {
                  sameDay: "hh:mm a",
                  lastDay: "[Yesterday]",
                  sameElse: "MMM, DD"
                })}
              />
              <Delete onClick={() => deletePost(post.objectID)} />
            </ListItemLink>
          );
        })}
    </List>
  );
};

export default Posts;
