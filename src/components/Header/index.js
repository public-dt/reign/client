import React from "react";

function Header() {
  return (
    <header className="">
      <h1>HN Feed</h1>
      <h2>{"We <3 hacker news!"}</h2>
    </header>
  );
}

export default Header;
